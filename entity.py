from dataclasses import dataclass
import faker

fake = faker.Faker()


@dataclass
class UserEntity:
    username: str
    password: str
    root: str
    token: str


class BlocEntity:
    def __init__(self, bid: str, parent: str) -> None:
        self.parent = parent
        self.id = bid


class DirectoryEntity(BlocEntity):
    def __init__(self, bid: str, parent: str) -> None:
        super().__init__(bid, parent)
        self.name: str = fake.file_path(depth=4).split("/")[1]

    def __repr__(self) -> str:
        return '(id={0}, name={1}, parent={2})'.format(self.id, self.name, self.parent)


class FileEntity(BlocEntity):
    def __init__(self, bid: str, parent: str) -> None:
        super().__init__(bid, parent)
        self.name: str = fake.file_name()

    def __repr__(self) -> str:
        return '(id={0}, name={1}, parent={2})'.format(self.id, self.name, self.parent)
